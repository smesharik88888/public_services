import json

import xmltodict


class XmlParser:
    """
    Класс для удобной работы с xml структурой.
    Сам парсинг осуществляется через xmltodict, далее идёт обработка для удобной работы.
    Можно работать как со словарём, так и через структуру.
    Все элементы итерируемые.
    """

    def __init__(self, xml:str):
        self.XML = xml
        self.__common_dict = dict()
        self.__init_on()

    def __str__(self):
        start = '----------------------Структура-----------------------\n'
        end =   '\n----------------------------------------------------\n'
        return start + str(json.dumps(self.__common_dict,indent=4, ensure_ascii=False)) + end

    def __init_on(self):
        """
        Метод, для получения словаря с данными из xml текста.
        :return:
        """
        self.__common_dict = json.loads(json.dumps(xmltodict.parse(self.XML)))

    def to_dict(self):
        """
        Метод для возвращения словаря с данными xml структуры.
        :return: словарь с данными.
        """

        return self.__common_dict

    @property
    def tree(self):
        """
        Свойство, которое содержит все теги структуры xml.
        :return: свойство структуры.
        """
        return XmlParser.Tag(self.__common_dict, 'tree')

    class Tag:
        """
        Класс, являющийся тегом в структуре xml.
        """

        def __init__(self, selected, key:str):
            self.__general_key = key
            self.__selected = selected
            self.iter_item = list()
            self.__createattributes()

        def __str__(self):
            # start = f'-------------Содержание тега "{self.__general_key}"---------------\n'
            # end = '\n------------------------------------------------------'
            return str(json.dumps(self.__selected,indent=4, ensure_ascii=False)) + '\n'

        def __getitem__(self, item):
            return self.__getattribute__(item)

        def __iter__(self):
            return iter(self.iter_item)

        def __createattributes(self):
            """
            Метод для создания атрибутов на основе тегов и наполнения их информацией по последующиму уровню.
            """

            keys = self.__selected.keys()
            for key in keys:

                if isinstance(self.__selected[key], dict):
                    self.__setattr__(key, XmlParser.Tag(self.__selected[key], key))
                elif isinstance(self.__selected[key], list):
                    tag_data: XmlParser.TagList = self.__generate_tag_data_for_list(self.__selected[key], key)
                    self.__setattr__(key, tag_data)
                else:
                    self.__setattr__(key, self.__selected[key])

                self.iter_item.append(
                    (key, self.__getattribute__(key))
                )

        def __generate_tag_data_for_list(self, selected, key):
            """
            метод для генирации тега как список.
            :param selected:
            :param key:
            :return:
            """
            tag_data = XmlParser.TagList()
            for item in selected:
                if isinstance(item, dict):
                    tag_data.append(XmlParser.Tag(item, key))
                elif isinstance(item, list):
                    tag_data.append(self.__generate_tag_data_for_list(item, key))
                else:
                    tag_data.append(item)

            return tag_data

        def to_dict(self):
            """
            Метод для возвращения словаря с данными xml структуры.
            :return: словарь с данными.
            """

            return self.__selected

    class TagList(list):
        """
        Класс, являющийся тегом(список) в структуре xml, наследуемый от класса list.
        """
        def __str__(self):
            start = '[\n'
            end = ']'
            body = ''.join([str(item) for item in iter(self)])
            return start + body + end



